QT       += core gui
QT       += sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    cpp/Adresse.cpp \
    cpp/clients.cpp \
    cpp/clientsException.cpp \
    cpp/cpp_to_qt.cpp \
    cpp/gestionclients.cpp \
    cpp/mouvements.cpp \
    cpp/operationmouvement.cpp \
    cpp/particulier.cpp \
    cpp/professionnels.cpp \
    main.cpp \
    mainwindow.cpp

HEADERS += \
    cpp/Adresse.h \
    cpp/clients.h \
    cpp/clientsException.h \
    cpp/cpp_to_qt.h \
    cpp/gestionclients.h \
    cpp/mouvements.h \
    cpp/operationmouvement.h \
    cpp/particulier.h \
    cpp/professionnels.h \
    mainwindow.h

FORMS += \
    mainwindow.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    ressourcebanque.qrc
