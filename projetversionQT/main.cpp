#include "mainwindow.h"
#include <QFile>
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    QFile fileCSS(":/mesStyles.css");

    bool openOK = fileCSS.open(QIODevice::ReadOnly);

    if (openOK)
    {
        QString infosCSS = fileCSS.readAll();
        a.setStyleSheet(infosCSS);
        fileCSS.close();
    }

    MainWindow w;
    w.show();
    return a.exec();
}
