#ifndef CPP_TO_QT_H
#define CPP_TO_QT_H

#include "cpp/Adresse.h"
#include "cpp/clients.h"
#include "cpp/particulier.h"
#include "cpp/professionnels.h"
#include "cpp/gestionclients.h"
#include "cpp/mouvements.h"

#include <QDebug>

class cpp_to_Qt
{
public:
    cpp_to_Qt();
    map<int,clients*> getclients();
    map<int,operationMouvement> getoperation();

private:
    gestionclients comptes;
    mouvements transaction;
};

#endif // CPP_TO_QT_H
