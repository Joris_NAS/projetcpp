#include "operationmouvement.h"

operationMouvement::operationMouvement(double CB,double retrait,double depot)
{
    //ctor
    Settotal_retrait(retrait);
    Settotal_CB(CB);
    Settotal_depot(depot);
    this->soldetotal=0;
}

operationMouvement::~operationMouvement()
{
    //dtor
}

void operationMouvement::ajouteroperation(int code, double montant)
{
    if (code==1){
        total_retrait +=montant;
        soldetotal-=montant;
    }
    if (code==2){
        total_CB +=montant;
        soldetotal-=montant;
    }
    if (code==3){
        total_depot +=montant;
        soldetotal+=montant;
    }
}
