#ifndef PROFESSIONNELS_H
#define PROFESSIONNELS_H

#include "clients.h"
#include <sstream>

enum statut {SARL, SA, SAS, EURL};

class professionnels : public clients
{
    public:
        professionnels(int,string,string, string ,string ,string,string,string,statut,string ,string ,string,string);
        virtual ~professionnels();

        string Getsiret() { return siret; }
        void Setsiret(string val);
        Adresse Getsiege() { return siege; }
        void Setsiege(Adresse val) { siege = val; }

        string GetStatut();
        void SetStatut(statut);

        virtual string Getinfosclient();

    protected:

    private:
        string siret;
        Adresse siege;
        statut juridique;
        string message;
        bool sameadresse;
};

#endif // PROFESSIONNELS_H
