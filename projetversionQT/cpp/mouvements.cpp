#include "mouvements.h"
#include <QDebug>

mouvements::~mouvements()
{
    //dtor
}

void mouvements::readfile()
{
    ifstream f("C:\\Users\\joris.nassala\\Desktop\\Reskilling\\QT\\projet_banque\\Operations.txt");
    if (!f)
    {
        //si fichier vide on sort
        return;
    }

    while (!f.eof())
    {
        int id(0),code(0);
        long date (0);
        double montant(0);
        char c;

        f>>id;
        f >> c;
        f>>date;
        f >> c;
        f>>code;
        f >> c;
        f>>montant;

        if (id !=0)
        {
            //on enregistre la ligne d'operations effectu� dans la map
            if (code < 4)
            {
                maptraitement[id].ajouteroperation(code,montant);

                qDebug() << "on est rentr� dans le readfile";

                /*cout << id << "," << date << "," << code << "," << montant << endl;
                stringstream ss;
                ss <<" Numero de compte :"<< id <<endl<<"Date Operation :"<<date<<endl<<"Code Operation :"<<code<<endl<<"Montant Operation :"<<montant<<endl;
                cout <<ss.str();*/
            }
            else
            {
                //log anomalie pour code operation erron�
            stringstream ss;
            ss << "Mauvais code: " << code << " at (" << id << " " << date
               << " "  << montant << ")" << endl;
            LogAnomalies(ss.str());

            }

        }
    }
    //Affichage du bilan des operations par compte
    for (auto it= begin(maptraitement); it!=end(maptraitement); it++)
    {
        // concatenation de l'affichage des operations par compte
        oss<<"Compte numero: " << it->first <<endl<<endl;
        oss<< "\tTotal Retrait de "<< it->second.Gettotal_retrait()<< " Euros"<<endl;
        oss << "\tTotal Depot de " << it->second.Gettotal_depot() << " Euros" << endl;
        oss << "\tTotal Carte Bleue de " << it->second.Gettotal_CB() << " Euros" << endl;
        oss << "\tTotal solde " << it->second.Getsoldtotal() << " Euros" << endl;

        }

}


string mouvements::toString()
{
return oss.str() ;
}

map<int, operationMouvement> mouvements::getTraitement() const
{
    qDebug() << "on est rentr� dans le gettraitement";
    return maptraitement;
}


void mouvements::LogAnomalies(string s)
{
    ofstream f("anomalies.txt", ofstream::out | ofstream::app);
    f << "At " << s;
    f.close();

}
