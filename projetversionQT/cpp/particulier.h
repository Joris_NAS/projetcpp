#ifndef PARTICULIER_H
#define PARTICULIER_H

#include "clients.h"
#include <sstream>

enum eSEXE {M,F};

class particulier : public clients
{
    public:
        particulier(int,string,string, string ,string ,string,string,string,eSEXE,string);
        virtual ~particulier();

        string Getprenom() { return prenom; }
        void Setprenom(string val) ;
        string Getbirthdate() { return birthdate; }
        void Setbirthdate(string val) { birthdate = val; }

        string GetSexe();
        void Setsexe(eSEXE);

        virtual string Getinfosclient();

        //ajout de l'adresse client
       // Particulier operator+(const Adresse*);

    protected:

    private:
        string prenom;
        string birthdate;
        eSEXE sexe;
        string message;
};

#endif // PARTICULIER_H
