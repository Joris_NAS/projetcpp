#ifndef ADRESSE_H
#define ADRESSE_H

#include <iostream>
#include <sstream>
//#include "clientException.h"

using namespace std;

class Adresse
{
    public:
        Adresse(string="numero de rue\0",string="complement\0",string="ville\0",string="codepostale");
        virtual ~Adresse();

        string Getlibeller() { return libeller; }
        void Setlibeller(string val) { libeller = val; }
        string Getcomplement() { return complement; }
        void Setcomplement(string val) { complement = val; }
        string Getcodepostale() { return codepostale; }
        void Setcodepostale(string val) { codepostale = val; }
        string Getville() { return ville; }
        void Setville(string val) { ville = val; }

        string toString();
        void affiche();

    protected:

    private:

        string libeller;
        string complement;
        string codepostale;
        string ville;

};

#endif // ADRESSE_H
