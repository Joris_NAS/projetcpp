#include "cpp_to_qt.h"

cpp_to_Qt::cpp_to_Qt()
{
    try
    {

        /******************************PARTIE 1**********************************************************/
        clients* collection[11];

        collection[0]= new professionnels(2,"AXA","125","rue LaFayette,Digicode 1432","FONTENAY SOUS BOIS","94120","info@axa.fr","12548795641122",statut::SARL,"125","rue LaFayette,Digicode 1432","FONTENAY SOUS BOIS","94120");
        collection[1]= new professionnels(4,"PAUL","36","quai des Orfèvres","OISSY EN France","93500","info@paul.fr","87459564455444",EURL,"12","rue des Oliviers","CRETEIL","92060");
        collection[2]= new professionnels(6,"PRIMARK","32","rue E. Renan, Bat. C","PARIS","75002","contact@primark.fr","08755897458455",SARL,"12","rue des Oliviers","CRETEIL","75002");
        collection[3]= new professionnels(8,"ZARA","23","av P. Valery","LA DEFENSE","92100","info@zara.fr","65895874587854",SA,"12","rue des Oliviers","CRETEIL","92060");
        collection[4]= new professionnels(10,"LEONIDAS","15","Place de la Bastille,Fond de Cour","PARIS","75003","contact@leonidas.fr","91235987456832",SAS,"10","rue de la Paix","PARIS","75008");

        collection[5]= new particulier(1,"BETY","12","rue des Oliviers","CRETEIL","94000","bety@gmail.com","Daniel",M,"11/12/1985");
        collection[6]= new particulier(3,"BODIN","10","rue des Olivies,Etage 2","VINCENNES","94300","bodin@gmail.com","Justin",M,"5/5/1965");
        collection[7]= new particulier(5,"BERRIS","15","rue de la République","FONTENAY SOUS BOIS","94120","berris@gmail.com","Karine",F,"6/6/1977");
        collection[8]= new particulier(7,"ABENIR","25", "rue de la Paix","LA DEFENSE",	"92100",	"abenir@gmail.com",	"Alexandra",F,"4/12/1977");
        collection[9]= new particulier(9,"BENSAID","3", "avenue des Parcs","ROISSY EN France","93500","bensaid@gmail.com",	"Georgia"	,F,"4/16/1976");
        collection[10]= new particulier(11,"ABABOU","3", "rue Lecourbe","BAGNOLET","93200","ababou@gmail.com","Teddy",M,"10/10/1970");


        // gestionclients ajout des comptes clients;

        for(int i(0);i<11;i++){

            comptes+collection[i];

        };


        //qDebug() << ;
        qDebug().noquote() << "nb de client est :" <<comptes.Getnbclient()<< endl;

        qDebug().noquote() << "--------------------------" << endl;

        //qDebug().noquote() << QString::fromStdString(comptes.affichertout());

        qDebug().noquote() << "--------------------------" << endl;

        //affichage d'un client à l'aide du numero client
        //comptes.afficheclient(2);

        clients* client4=comptes.getclient(4);
        qDebug().noquote() << QString::fromStdString( client4->Getinfosclient());


        transaction.readfile();

        qDebug() << "on vient de faire  le readfile";


    }
    catch(clientsException& ex)
    {
        //cout << "clients Exception : " << ex.what() << endl;
        //cout << ex.Getmessage() << endl;
    }
    catch(exception& ex)
    {
        //cout << "Exception : " << ex.what() << endl;
    }
    catch(...)
    {
        //cout << "Autre Erreur !! " << endl;
    }
    // return 0;
}

map<int, clients *> cpp_to_Qt::getclients()
{
    return comptes.getMapclient();
}

map<int,operationMouvement> cpp_to_Qt::getoperation()
{
    qDebug() << "on est rentré dans le getoperation";
    return transaction.getTraitement();
}
