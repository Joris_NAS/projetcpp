#include "professionnels.h"

professionnels::professionnels(int id ,string name,string lib,string complementadresse,string city,string CP,string email,string s, statut stat, string lib1,string complementadresse1,string city1,string CP1)
:clients(id,name,lib,complementadresse, city, CP,email)
{
    Adresse AD1(lib1,complementadresse1, city1, CP1);

    //comparaison des adresses postale du siege et perso
    if (AD1.Getlibeller()==lib && AD1.Getcomplement()==complementadresse
        && AD1.Getville()==city && AD1.Getcodepostale()==CP)
    {
        sameadresse=true;
        this->Setsiege(AD1);
    }
    else {
        sameadresse=false;
        this->Setsiege(AD1);
    }

    this->SetStatut(stat);
    this->Setsiret(s);

}

professionnels::~professionnels()
{
    //cout << "Destruction du professionnel " << endl;
}

void professionnels::Setsiret(string val)
{
    if (val.size()<15){
        siret=val;
    }
    else{
        cout<<"ERREUR siret sur client"<< Getnom()<<endl;
       throw clientsException(mesErreurs::ERR_SIRET);
    }
}


string professionnels::GetStatut()
{
      switch(juridique)
    {
        case statut::EURL:
            this-> message= " EURL ";
            break;

         case statut::SA:
            this->message = " --SA-- ";
            break;

         case statut::SARL:
            this->message= " --SARL-- ";
            break;

         case statut::SAS:
            this->message= " --SAS-- ";
            break;

         default:
            this->message= " Statut juridique inconnu";
            break;
        }
        return this->message;
}

void professionnels::SetStatut(statut stat)
{

    juridique=stat;
}

string professionnels::Getinfosclient()
{
    ostringstream o;
    o <<"Professionel:"<<Getidentifiant()<<endl;
    o <<"\t"<<"Siret:"<<Getsiret()<<endl;
    o <<"\t"<<GetStatut()<<" "<<Getnom()<<endl;
    //cout <<"\t"<<"Adresse perso"<<endl;
    o << clients::toString();
    if (sameadresse==false){
        o <<"\t"<<"Siege:"<<endl;
        o << Getsiege().toString();
    }
return o.str();

}
