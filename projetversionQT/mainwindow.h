#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QCloseEvent>
#include "cpp/cpp_to_qt.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    bool initComposantsprivate();
    bool initComposantspro();

private slots:
    void on_pushButton_clicked();

    void on_pushclientpriv_clicked();

    void on_pushclientpro_clicked();

    void closeEvent(QCloseEvent *event);
    void on_actionQuitter_triggered();

private:
    Ui::MainWindow *ui;
    cpp_to_Qt cpp;
};
#endif // MAINWINDOW_H
