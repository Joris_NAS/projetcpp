#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QApplication>
#include <QMessageBox>
#include <QSqlDatabase>
#include <QDebug>
#include <QFile>
#include <QSqlQuery>
#include <QDateTime>
#include <QSqlTableModel>
#include <QTableWidget>

#include "cpp/Adresse.h"
#include "cpp/clients.h"
#include "cpp/particulier.h"
#include "cpp/professionnels.h"
#include "cpp/gestionclients.h"
#include "cpp/mouvements.h"


MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{

    ui->setupUi(this);

    ui->groupcompte->setVisible(false);
    ui->groupclientpriv->setVisible(false);
    ui->groupclientpro->setVisible(false);



    initComposantsprivate();

    ostringstream o;
    auto op=cpp.getoperation();

    for (map<int, operationMouvement>::iterator it=op.begin();     //poistion au debut de la map
            it!=op.end();                                           //Verifie que la fin de ma map n'est pas atteinte
            ++it)                                                       //Increment permet de passer
    {
        qDebug() << "on est rentré";
        // concatenation de l'affichage des operations par compte
        o<<"Compte numero: " << it->first <<endl<<endl;
        o<< "\tTotal Retrait de "<< it->second.Gettotal_retrait()<< " Euros"<<endl;
        o << "\tTotal Depot de " << it->second.Gettotal_depot() << " Euros" << endl;
        o << "\tTotal Carte Bleue de " << it->second.Gettotal_CB() << " Euros" << endl;
        o << "\tTotal solde " << it->second.Getsoldtotal() << " Euros" << endl;;
        o << endl << "########################" << endl;
    }

qDebug().noquote() << QString::fromStdString(o.str());

    initComposantspro();

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    QMessageBox msgBox;
    msgBox.setText("Quitter ?");
    msgBox.setInformativeText(" Etes vous sûr(e) de vouloir quitter ?");
    msgBox.setStandardButtons(QMessageBox::Yes | QMessageBox::No | QMessageBox::Cancel);
    msgBox.setIcon(QMessageBox::Icon::Question);
    msgBox.setDefaultButton(QMessageBox::Cancel);

        int reponse = msgBox.exec();
    switch(reponse)
    {
        case QMessageBox::Yes :
                event->accept(); //Valider l'event de fermeture
            break;
        default:
                event->ignore(); //Annuler l'event de fermeture
            break;
    }
}


bool MainWindow::initComposantsprivate()
{
    //init entete de clients privé
    qDebug()<< "on est la ";

    QFile f(":/tableclientprivateheader/privateheaders.txt");
    if(!f.open(QIODevice::ReadOnly))
    {
        return false;
    }
    ui->tableclientpriv->clear();
    ui->tableclientpriv->setRowCount(0);

    QString alltext=f.readAll();
    auto lines = alltext.split("\r\n");

    ui->tableclientpriv->setColumnCount(lines.count());

    for(int i=0; i<lines.count();i++)
    {
        ui->tableclientpriv->setHorizontalHeaderItem(i,new QTableWidgetItem(lines[i], 0));
        qDebug()<<lines[i];
    }
    ui->tableclientpriv->resizeColumnsToContents();
 return true;
}


bool MainWindow::initComposantspro()
{
    //init entete de clients privé
    qDebug()<< "on est la ";

    QFile f(":/proheaders.txt");
    if(!f.open(QIODevice::ReadOnly))
    {
        return false;
    }
    ui->tableclientpro->clear();
    ui->tableclientpro->setRowCount(0);

    QString alltext=f.readAll();
    auto lines = alltext.split("\r\n");

    ui->tableclientpro->setColumnCount(lines.count());

    for(int i=0; i<lines.count();i++)
    {
        ui->tableclientpro->setHorizontalHeaderItem(i,new QTableWidgetItem(lines[i], 0));
        qDebug()<<lines[i];
    }
    ui->tableclientpro->resizeColumnsToContents();
 return true;
}

void MainWindow::on_pushButton_clicked()
{
 ui->groupcompte->setVisible(true);
 ui->groupclientpriv->setVisible(false);
 ui->groupclientpro->setVisible(false);

QSqlDatabase db;
db = QSqlDatabase::addDatabase("QSQLITE");


if (QFile("C:\\Users\\joris.nassala\\Desktop\\Reskilling\\QT\\projet_banque\\BdComptes.db").exists())
    {
    db.setDatabaseName("C:\\Users\\joris.nassala\\Desktop\\Reskilling\\QT\\projet_banque\\BdComptes.db");
    bool BdcOk = db.open();

    if (BdcOk == true)
        {
        //creation d'un tableau de donnée provenant de la base de donner
        QSqlQueryModel  *modelCompte = new QSqlQueryModel();
        modelCompte->setQuery("select numcompte, datecreation, solde, decouvert, numcli  From comptes order by numcli;");
        //Renommer les entetes de Colonnes
        modelCompte->setHeaderData(0, Qt::Horizontal,  "Numéro compte");
        modelCompte->setHeaderData(1, Qt::Horizontal,  "date creation");
        modelCompte->setHeaderData(2, Qt::Horizontal,  "solde compte");
        modelCompte->setHeaderData(3, Qt::Horizontal,  "Decouvert ");
        modelCompte->setHeaderData(4, Qt::Horizontal,  "Numero Client");

        ui->tablecompte->setModel(modelCompte);
        ui->tablecompte->setSelectionMode(QAbstractItemView::SingleSelection); // 1 Seule Selection
        ui->tablecompte->setSelectionBehavior(QAbstractItemView::SelectRows); // Selection de la ligne entière
        ui->tablecompte->setEditTriggers(QAbstractItemView::NoEditTriggers); //Interdit la Modification
        }
    db.close();

    }
    else {
        qDebug() << "Echec Connexion !!!";
    }

}

void MainWindow::on_pushclientpriv_clicked()
{
    ui->groupcompte->setVisible(false);
    ui->groupclientpriv->setVisible(true);
    ui->groupclientpro->setVisible(false);
    ui->tableclientpriv->clear();

    auto cli=cpp.getclients();


    for (map<int, clients*>::iterator it=cli.begin();     //poistion au debut de la map
            it!=cli.end();                                //Verifie que la fin de ma map n'est pas atteinte
            ++it)                                         //Increment permet de passer
    {
        auto pers = it->second;
        auto clipriv = dynamic_cast<particulier*>(pers);

        if(clipriv){

            ui->tableclientpriv->insertRow(ui->tableclientpriv->rowCount());
            auto idx = ui->tableclientpriv->rowCount()-1;

            qDebug() << "on range les clients privé dans le tableau privé";
            ui->tableclientpriv->setItem(idx, 0, new QTableWidgetItem(QString::number(clipriv->Getidentifiant())));
            ui->tableclientpriv->setItem(idx, 1, new QTableWidgetItem(QString::fromStdString(clipriv->Getnom())));
            ui->tableclientpriv->setItem(idx, 2, new QTableWidgetItem(QString::fromStdString(clipriv->Getadresse().Getlibeller())));
            ui->tableclientpriv->setItem(idx, 3, new QTableWidgetItem(QString::fromStdString(clipriv->Getadresse().Getcomplement())));
            ui->tableclientpriv->setItem(idx, 4, new QTableWidgetItem(QString::fromStdString(clipriv->Getadresse().Getville())));
            ui->tableclientpriv->setItem(idx, 5, new QTableWidgetItem(QString::fromStdString(clipriv->Getadresse().Getcodepostale())));
            ui->tableclientpriv->setItem(idx, 6, new QTableWidgetItem(QString::fromStdString(clipriv->GetMail())));
            ui->tableclientpriv->setItem(idx, 7, new QTableWidgetItem(QString::fromStdString(clipriv->Getprenom())));
            ui->tableclientpriv->setItem(idx, 8, new QTableWidgetItem(QString::fromStdString(clipriv->GetSexe())));
            ui->tableclientpriv->setItem(idx, 9, new QTableWidgetItem(QString::fromStdString(clipriv->Getbirthdate())));
            ui->tableclientpriv->resizeColumnsToContents();

        }
    }
}

void MainWindow::on_pushclientpro_clicked()
{
    ui->groupcompte->setVisible(false);
    ui->groupclientpriv->setVisible(false);
    ui->groupclientpro->setVisible(true);

    ui->tableclientpro->clear();
    auto cli=cpp.getclients();


    for (map<int, clients*>::iterator it=cli.begin();     //poistion au debut de la map
            it!=cli.end();                                //Verifie que la fin de ma map n'est pas atteinte
            ++it)                                         //Increment permet de passer
    {
        auto pers = it->second;
        auto clipro = dynamic_cast<professionnels*>(pers);

        if(clipro){
            ui->tableclientpro->insertRow(ui->tableclientpro->rowCount());
            auto idx = ui->tableclientpro->rowCount()-1;

            qDebug() << "on range les clients professionnel dans le tableau pro";
            ui->tableclientpro->setItem(idx, 0, new QTableWidgetItem(QString::number(clipro->Getidentifiant())));
            ui->tableclientpro->setItem(idx, 1, new QTableWidgetItem(QString::fromStdString(clipro->Getnom())));
            ui->tableclientpro->setItem(idx, 2, new QTableWidgetItem(QString::fromStdString(clipro->Getadresse().Getlibeller())));
            ui->tableclientpro->setItem(idx, 3, new QTableWidgetItem(QString::fromStdString(clipro->Getadresse().Getcomplement())));
            ui->tableclientpro->setItem(idx, 4, new QTableWidgetItem(QString::fromStdString(clipro->Getadresse().Getville())));
            ui->tableclientpro->setItem(idx, 5, new QTableWidgetItem(QString::fromStdString(clipro->Getadresse().Getcodepostale())));
            ui->tableclientpro->setItem(idx, 6, new QTableWidgetItem(QString::fromStdString(clipro->GetMail())));
            ui->tableclientpro->setItem(idx, 7, new QTableWidgetItem(QString::fromStdString(clipro->Getsiret())));
            ui->tableclientpro->setItem(idx, 8, new QTableWidgetItem(QString::fromStdString(clipro->GetStatut())));
            ui->tableclientpro->setItem(idx, 9, new QTableWidgetItem(QString::fromStdString(clipro->Getsiege().Getlibeller())));
            ui->tableclientpro->setItem(idx, 10, new QTableWidgetItem(QString::fromStdString(clipro->Getsiege().Getcomplement())));
            ui->tableclientpro->setItem(idx, 11, new QTableWidgetItem(QString::fromStdString(clipro->Getsiege().Getville())));
            ui->tableclientpro->setItem(idx, 12, new QTableWidgetItem(QString::fromStdString(clipro->Getsiege().Getcodepostale())));
            ui->tableclientpro->resizeColumnsToContents();

        }
    }

}

void MainWindow::on_actionQuitter_triggered()
{
    close();
}
