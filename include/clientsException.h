#ifndef CLIENTSEXCEPTION_H
#define CLIENTSEXCEPTION_H

#include <exception>

#include <iostream>
#include <sstream>

using namespace std;

enum mesErreurs {ERR_MAIL,ERR_SIRET,ERR_NOM,ERR_PRENOM};

class clientsException : public exception
{
    public:
        clientsException(mesErreurs)throw();
        virtual ~clientsException();

        mesErreurs GetcodeErreur() { return codeErreur; }

        string Getmessage() const ;
        const char* what() const throw();

    protected:
            void SetcodeErreur(mesErreurs val){codeErreur=val;}
    private:
        mesErreurs codeErreur;
        mutable string message;
};

#endif // CLIENTSEXCEPTION_H
