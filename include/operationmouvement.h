#ifndef OPERATIONMOUVEMENT_H
#define OPERATIONMOUVEMENT_H

/****** classe recapitulant les mouvement banquaire par compte ****/
class operationMouvement
{
    public:
        operationMouvement(double= 0,double= 0,double= 0);
        ~operationMouvement();

        double Gettotal_CB() { return total_CB; }
        void Settotal_CB(double val) { total_CB = val; }
        double Gettotal_retrait() { return total_retrait; }
        void Settotal_retrait(double val) { total_retrait = val; }
        double Gettotal_depot() { return total_depot; }
        void Settotal_depot(double val) { total_depot = val; }
        double Getsoldtotal() { return soldetotal; }
        void ajouteroperation(int,double);

    protected:

    private:
        double total_CB=0;
        double total_retrait=0;
        double total_depot=0;
        double soldetotal=0;
};

#endif // OPERATIONMOUVEMENT_H
