#ifndef GESTIONCLIENTS_H
#define GESTIONCLIENTS_H

#include "clients.h"
#include <map>
#include <sstream>

class gestionclients
{
    public:
        gestionclients();
        virtual ~gestionclients();

        int Getnbclient() { return mapclient.size(); }
        clients* getclient(int val){return mapclient.at(val);}
        void afficheclient(int ID);
        //ajouter un clients � la map
        void operator+ (clients*);

        string affichertout();

    private:

    map<int, clients*> mapclient;
};

#endif // GESTIONCLIENTS_H
