#ifndef MOUVEMENTS_H
#define MOUVEMENTS_H

#include "operationmouvement.h"

#include <iostream>
#include <fstream>
#include <iomanip>
#include <sstream>
using namespace std;

#include <map>


class mouvements
{
    public:
        mouvements(){}
        ~mouvements();

        void readfile();
        void AfficherOperation();
        void LogAnomalies(string);

        string toString();

    protected:

    private:
        ostringstream oss;
       /* int operationnum;
        string operationdate;
        int operationcode;
        double operationmontant;*/

};


#endif // MOUVEMENTS_H
