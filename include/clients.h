#ifndef CLIENTS_H
#define CLIENTS_H

#include <sstream>
#include <iostream>
#include "clientsException.h"
#include "Adresse.h"

using namespace std;

class clients
{
    public:
        clients(int,string,string, string ,string ,string ,string );
        virtual ~clients();

        int Getidentifiant() { return identifiant; }
        void Setidentifiant( int val) { identifiant = val; }
        string Getnom() { return nom; }
        void Setnom(string val);

        int verifymail(string);
        void SetMail(string);
        string GetMail();

        Adresse Getadresse() { return adresssClient; }
        void Setadresse( Adresse AD) { adresssClient = AD; }



        void infosclient();
        string toString();
        virtual string Getinfosclient()=0;

    protected:

    private:
        int identifiant;
        string nom;
        Adresse adresssClient;
        string mail;
};

#endif // CLIENTS_H
