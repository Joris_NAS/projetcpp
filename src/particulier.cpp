#include "particulier.h"

particulier::particulier(int id ,string name,string lib,string complementadresse,string city,string CP,string email, string vprenom, eSEXE genre, string birth):clients(id,name,lib,complementadresse, city, CP,email)
{

    this->Setprenom(vprenom);
    this->Setsexe(genre);
    this->Setbirthdate(birth);

}

particulier::~particulier()
{
  // cout << "Destruction du particulier " << endl;
}

void particulier::Setprenom(string val)
{
    if (val.size()<=50){
     prenom = val;
    }
    else
    {
        throw clientsException(mesErreurs::ERR_PRENOM);
    }

}


string particulier::GetSexe()
{
    switch(sexe)
    {
        case eSEXE::F:
            this->message= "Mr/Mrs. ";
            break;

         case eSEXE::M:
            this->message= "M.";
            break;

         default:
            this->message= "sexe inconnu";
            break;
        }
        return this->message;
}


void particulier::Setsexe(eSEXE genre)
{
    sexe= genre;

}

string particulier::Getinfosclient()
{
    ostringstream o;
    o<<"Particulier: "<<Getidentifiant()<<endl;
    o<<"\t"<<GetSexe()<<Getnom()<<" "<<Getprenom()<<endl;
    o<< clients::toString();
    o<<"\t"<<"Sexe: "<< GetSexe()<<endl;
    o<<"\t"<<"date de naissance: "<<Getbirthdate()<<endl;

    return o.str();
}
