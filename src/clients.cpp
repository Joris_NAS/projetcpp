#include "clients.h"
#define LG 50

clients::clients(int id ,string name, string lib,string complementadresse,string city,string CP,string email)
{
// ajout de l'identifiant client
    this->Setidentifiant(id);
//ajout nom du client
    this ->Setnom(name);

// ajout du mail client
    this->SetMail(email);

    Adresse AD(lib,complementadresse, city, CP);
    Setadresse(AD);

}

clients::~clients()
{
    cout << "Destruction du client " << identifiant << endl;
}

void clients::Setnom(string val)
{
    if (val.size()<=LG){
     nom=val;
    }
    else
    {
        throw clientsException(mesErreurs::ERR_NOM);
    }
}


int clients::verifymail(string email)
{
    int dot = 0;

    for ( int i(0); i<email.size();i++)
    {
        if ( email[i]=='@'){
            dot++;
        }
    }
    return dot;
}

void clients::SetMail(string email)
{
    if ( verifymail(email)>0 && verifymail(email)<2){
            mail=email;
        }
    else {
            cout<<"ERREUR Mail contient :"<<verifymail(email)<<" @ pour client"<< Getnom()<<endl;
            throw clientsException(mesErreurs::ERR_MAIL);

        }
}

string clients::GetMail()
{
    return mail;
}


void clients::infosclient()
{
    adresssClient.affiche();
    //cout <<"ID: "<<identifiant<<endl<<"Nom: "<<Getnom()<<endl;
   // cout<<"\t"<<"Mail: "<<GetMail()<<endl;

}

string clients::toString()
{
    ostringstream o;

    o << adresssClient.toString();
    o << "\t"<<"Mail: "<<GetMail()<<endl;
    return o.str();
}


